@extends('adminlte.master')

@section('title')
<h1>Tambah mobil</h1>
@endsection

@section('content')
<form action="/mobils" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="merek">merek</label>
        <input type="text" class="form-control" name="merek" id="merek" placeholder="Masukkan merek">
        @error('merek')
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="jenis_">jenis_</label>
        <input type="text" class="form-control" name="jenis_mobil" id="jenis_mobil" placeholder="Masukkan jenis_mobil">
        @error('jenis_mobil')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="kapasitas">kapasitas</label>
        <input type="text" class="form-control" name="kapasitas" id="kapasitas" placeholder="Masukkan kapasitas">
        @error('kapasitas')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="foto">foto</label>
        <input type="file" class="form-control" name="foto" id="foto" >
        @error('foto')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection