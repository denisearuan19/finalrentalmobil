@extends('adminlte.master')

@section('title')
<h1>List pelanggan</h1>
@endsection

@section('content')
<a href="/pelanggans/create" class="btn btn-primary mb-2">Tambah</a>

<div class="row">
            @foreach ( $listpelanggan as $item)
                <div class="col-mb-4">
                    <div class="card-columns-fluid" style="width:15rem;">
                        <img src="{{asset('uploads/pelanggans/'.$item->foto)}}" class="card-img-top" alt="..." >
                        <div class="card-body">
                            <h4>nama : {{$item->nama}}</h4><br>
                            <h4>no_KTP : {{$item->no_KTP}}</h4>
                            <h4>no_telp : {{$item->no_telp}}</h4>
                            <p>{{$item->foto}}</p>
                            <form action="/pelanggans/{{$item->id}}" method="POST">
                                <a href="/pelanggans/{{$item->id}}" class="btn btn-info">Show</a>
                                <a href="/pelanggans/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </div>
                    </div> 
                </div>
            @endforeach
</div> 
@endsection
