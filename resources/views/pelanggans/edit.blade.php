@extends('adminlte.master')

@section('title')
<h1>Edit pelanggan</h1>
@endsection

@section('content')
<form action="/pelanggans/{{$listpelanggan->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="no_KTP">no_KTP</label>
        <input type="text" class="form-control" value="{{$listpelanggan->no_KTP}}" name="no_KTP" id="no_KTP" placeholder="Masukkan no_KTP">
        @error('no_KTP')
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="nama">nama</label>
        <input type="text" class="form-control" value="{{$listpelanggan->nama}}" name="nama" id="nama" placeholder="Masukkan nama">
        @error('nama')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label for="alamat">alamat</label>
        <input type="text" class="form-control" value="{{$listpelanggan->alamat}}" name="alamat" id="alamat" placeholder="Masukkan alamat">
        @error('alamat')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="tempat_lahir">tempat_lahir</label>
        <input type="text" class="form-control" value="{{$listpelanggan->tempat_lahir}}" name="tempat_lahir" id="tempat_lahir" placeholder="Masukkan tempat_lahir">
        @error('tempat_lahir')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="tanggal_lahir">tanggal_lahir</label>
        <input type="date" class="form-control" value="{{$listpelanggan->tanggal_lahir}}" name="tanggal_lahir" id="tanggal_lahir" placeholder="Masukkan tanggal_lahir">
        @error('tanggal_lahir')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="no_telp">no_telp</label>
        <input type="text" class="form-control" value="{{$listpelanggan->no_telp}}" name="no_telp" id="no_telp" placeholder="Masukkan no_telp">
        @error('no_telp')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="foto">foto</label>
        <input type="file" class="form-control" value="{{$listpelanggan->foto}}"  name="foto" id="foto" >
        @error('foto')
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection