<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\mobil;
use File;

class MobilController extends Controller
{

    public function index()
    {
        $listmobil = mobil::all();
        return view('mobils.index', compact('listmobil'));
    }


    public function create()
    {
        return view('mobils.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'merek' => 'required',
            'jenis_mobil' => 'required',
            'kapasitas' => 'required',
            'foto' => 'required|mimes:jpeg,jpg,png|max:2200'
        ]);

        $foto = $request->foto;
        $new_foto = time() . '-' . $foto->getClientOriginalName();


        mobil::create([
            'merek' => $request->merek,
            'jenis_mobil' => $request->jenis_mobil,
            'kapasitas' => $request->kapasitas,
            'foto' => $new_foto
        ]);
        $foto->move('uploads/mobils/', $new_foto);
        return redirect('/mobils');
    }

    public function show($id)
    {
        $listmobil = mobil::find($id);
        return view('mobils.show', compact('listmobil'));
    }

    public function edit($id)
    {
        $listmobil = mobil::find($id);
        return view('mobils.edit', compact('listmobil'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'merek' => 'required',
            'jenis_mobil' => 'required',
            'kapasitas' => 'required',
            'foto' => 'mimes:jpeg,jpg,png|max:2200',
        ]);
        $listmobil = mobil::findorfail($id);
        if ($request->has('foto')) {
            $path = "uploads/mobils/";
            File::delete($path, $listmobil->foto);
            $foto = $request->foto;
            $new_foto = time() . '-' . $foto->getClientOriginalName();
            $foto->move('uploads/mobils', $new_foto);
            $post_data = [
                'merek' => $request->merek,
                'jenis_mobil' => $request->jenis_mobil,
                'kapasitas' => $request->kapasitas,
                'foto' => $new_foto,
            ];
        } else {
            $post_data = [
                'merek' => $request->merek,
                'jenis_mobil' => $request->jenis_mobil,
                'kapasitas' => $request->kapasitas,
            ];
        }
        $listmobil->update($post_data);
        return redirect('/mobils');
        //     return redirect()->route('mobils.index')->with('success', 'Mobil berhasil diUpdate!');
    }



    // $request->validate([
    //     'merek' => 'required',
    //     'jenis_mobil' => 'required',
    //     'kapasitas' => 'required',
    //     'foto' => 'required'
    // ]);
    // $listmobil = mobil::find($id);
    // $listmobil->merek = $request->merek;
    // $listmobil->jenis_mobil = $request->jenis_mobil;
    // $listmobil->kapasitas = $request->kapasitas;
    // $listmobil->foto = $request->foto;
    // return redirect('/mobils');


    public function destroy($id)
    {
        // mobil::destroy($id);
        // return redirect('/mobils')->with('success', 'Mobil Berhasil dihapus!');
        $listmobil = mobil::findorfail($id);
        $listmobil->delete();

        $path = "uploads/mobils";
        File::delete($path . $listmobil->foto);

        return redirect()->route('mobils.index');
    }
}