<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\profile;
use File;

class ProfileController extends Controller
{
   
    public function index()
    {
        $listprofile = profile::all();
        return view('profiles.index', compact('listprofile'));
    }

   
    public function create()
    {
        return view('profiles.create');
    }

  
    public function store(Request $request)
    {
        
    }

   
    public function show($id)
    {
        $listprofile = profile::find($id);
        return view('profiles.show', compact('listprofile'));
    }

  
    public function edit($id)
    {
        $listprofile = profile::find($id);
        return view('profiles.edit', compact('listprofile'));
    }

  
    public function update(Request $request, $id)
    {
        //
    }

 
    public function destroy($id)
    {
        profile::destroy($id);
        return redirect('/profiles')->with('success', 'profile Berhasil dihapus!');
    }
}