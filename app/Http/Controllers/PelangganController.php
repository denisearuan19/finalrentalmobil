<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\pelanggan;
use File;

class PelangganController extends Controller
{
    
    public function index()
    {
        $listpelanggan = pelanggan::all();
        return view('pelanggans.index', compact('listpelanggan'));
   
    }

  
    public function create()
    {
        return view('pelanggans.create');
    }

 
    public function store(Request $request)
    {
        $this->validate($request, [
            // 'no_KTP' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'no_telp' => 'required',
            'foto' => 'required|mimes:jpeg,jpg,png|max:2200'
        ]);

        $foto = $request->foto;
        $new_foto = time() . '-' . $foto->getClientOriginalName();


        pelanggan::create([
            // 'no_KTP' => $request->no_KTP,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'no_telp' => $request->no_telp,
            'foto' => $new_foto
        ]);
        $foto->move('uploads/pelanggans/', $new_foto);
        return redirect('/pelanggans');
    }

 
    public function show($id)
    {
        $listpelanggan = pelanggan::find($id);
        return view('pelanggans.show', compact('listpelanggan'));
    }

  
    public function edit($id)
    {
        $listpelanggan = pelanggan::find($id);
        return view('pelanggans.edit', compact('listpelanggan'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'no_KTP' => 'required',
            'nama' => 'required',
            'alamat' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'no_telp' => 'required',
            'foto' => 'required|mimes:jpeg,jpg,png|max:2200'
        ]);
        $listpelanggan = pelanggan::findorfail($id);
        if ($request->has('foto')) {
            $path = "uploads/pelanggans/";
            File::delete($path, $listpelanggan->foto);
            $foto = $request->foto;
            $new_foto = time() . '-' . $foto->getClientOriginalName();
            $foto->move('uploads/pelanggans', $new_foto);
            $post_data = [
                'no_KTP' => $request->no_KTP,
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'no_telp_lahir' => $request->no_telp_lahir,
                'foto' => $new_foto
            ];
        } else {
            $post_data = [
                'no_KTP' => $request->no_KTP,
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'no_telp_lahir' => $request->no_telp_lahir,
            ];
        }
        $listpelanggan->update($post_data);
        return redirect('/pelanggans');
    }

  
    public function destroy($id)
    {
        pelanggan::destroy($id);
        return redirect('/pelanggans')->with('success', 'pelanggan Berhasil dihapus!');
    }
}